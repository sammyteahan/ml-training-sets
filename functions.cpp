/**
 * Helper functions
 */
#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <map>
#include <list>
#include <iterator>

#include "functions.hpp"

std::string removePunctuation(std::string &word) {
  for (int i = 0; i < word.length(); i++) {
    if (ispunct(word[i])) {
      word.erase(i, 1);
    }
  }

  return word;
}

std::vector<std::string> readFileToVector(const std::string &filename) {
  std::ifstream source;
  source.open(filename);
  std::vector<std::string> words;
  std::string word;

  while (source >> word) {
    removePunctuation(word);
    // @todo split hyphenated words for improved model accuracy
    words.push_back(word);
  }

  return words;
}

std::set<std::string> readFileToSet(const std::string &filename) {
  std::ifstream source;
  source.open(filename);
  std::set<std::string> words;
  std::string word;

  while (source >> word) {
    removePunctuation(word);
    words.insert(word);
  }

  return words;
}

std::map<std::string, std::string> generateMap(const std::vector<std::string> &vector) {
  std::map<std::string, std::string> map;
  std::string last = "";

  for (std::vector<std::string>::const_iterator it = vector.begin(); it != vector.end(); it++) {
    map[last] = *it;
    last = *it;
  }

  return map;
}

std::map<std::string, std::vector<std::string>> generateBetterMap(const std::vector<std::string> &vector) {
  std::map<std::string, std::vector<std::string>> map;
  std::string state = "";

  for (std::vector<std::string>::const_iterator it = vector.begin(); it != vector.end(); it++) {
    map[state].push_back(*it);
    state = *it;
  }

  return map;
}

void displayContents(const std::vector<std::string> &vector) {
  for (auto i: vector) {
    std::cout << i << std::endl;
  }
}

void displayMapContents(const std::map<std::string, std::string> &map) {
  for (auto const i: map) {
    std::cout << i.first << ", " << i.second << std::endl;
  }
}

void printContent(std::map<std::string, std::string> &map) {
  std::string state = "";
  std::cout << std::endl;

  for (int i = 0; i < 100; i++) {
    std::cout << map[state] << " ";
    state = map[state];
  }
  std::cout << std::endl;
}

void printContentAssociatedWithKey(std::map<std::string, std::vector<std::string>> &map) {
  std::srand(time(NULL));
  std::string state = "";

  for (int i = 0; i < 100; i++) {
    int index = rand() % map[state].size();
    std::cout << map[state][index] << " ";
    state = map[state][index];
  }
  std::cout << std::endl;
}

void printNephiContent(const std::map<std::string, std::vector<std::string>> &map) {
  for (auto const i: map) {
    if (i.first == "Nephi") {
      for (auto const j: i.second) {
        std::cout << j << ", ";
      }
    }
  }
  std::cout << std::endl;
}

std::string getFileFromProgram() {
  std::cout << "Enter the name of the file: ";
  std::string name;
  std::cin >> name;

  return name;
}

/**
 * @todo check to see if argument exists and provide
 * helpful error and return if that's the case
 */
std::string getFileFromStandardInput(char **argv) {
  std::string filename(argv[1]);

  return filename;
}

void writeSetToFile(const std::set<std::string> &set) {
  std::string outfileName;
  std::cout << "filename to write contents to: (e.g. out.txt) ";
  std::cin >> outfileName;

  std::ofstream outfile;
  outfile.open(outfileName);

  for (auto i: set) {
    outfile << i << std::endl;
  }

  return outfile.close();
}

void writeVectorToFile(const std::vector<std::string> &vector) {
  std::string outfileName;
  std::cout << "filename to write contents to: (e.g. out.txt) ";
  std::cin >> outfileName;

  std::ofstream outfile;
  outfile.open(outfileName);

  for (auto i: vector) {
    outfile << i << std::endl;
  }

  return outfile.close();
}

void writeMapToFile(const std::map<std::string, std::string> &map) {
  std::string outfileName;
  std::cout << "filename to write contents to: (e.g. out.txt) ";
  std::cin >> outfileName;

  std::ofstream outfile;
  outfile.open(outfileName);

  for (auto const i: map) {
    outfile << i.first << ", " << i.second << std::endl;
  }

  return outfile.close();
}
