##
# MAKEFILE
#

main: main.o functions.o
	g++ -o program main.o functions.o

main.o: main.cpp
	g++ -std=c++11 -c main.cpp

functions.o: functions.cpp functions.hpp
	g++ -std=c++11 -c functions.cpp

clean:
	rm *.o
	rm program
