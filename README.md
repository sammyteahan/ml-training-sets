## Maps and Sets for Machine Learning

This is a pretty brief exercise of what a training model might look like for machine learning. It's a c++ project so there's some crap we gotta worry about:

###### Getting started

```
git clone https://gitlab.com/sammyteahan/ml-training-sets.git
cd ml-training-sets
make
./program _nephi_trump.txt
```

###### Cleaning up

```
make clean
```

###### Notes

The code here isn't that great. It's built out to show a couple different ways of using maps, sets, and vectors in c++ so our main method contains some commented out code. To walk through the different steps, it necessary to comment/uncomment and re-compile using `make`.
