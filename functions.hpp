/**
 * Helper functions
 */
#ifndef _FUNCTIONS_HPP
#define _FUNCTIONS_HPP
#include <iostream>
#include <fstream>
#include <vector>
#include <set>
#include <map>

std::string removePunctuation(std::string &word);
std::vector<std::string> readFileToVector(const std::string &filename);
std::set<std::string> readFileToSet(const std::string &filename);
std::map<std::string, std::string> generateMap(const std::vector<std::string> &v);
std::map<std::string, std::vector<std::string>> generateBetterMap(const std::vector<std::string> &v);

std::string getFileFromProgram();
std::string getFileFromStandardInput(char **argv);

void displayContents(const std::vector<std::string> &v);
void displayMapContents(const std::map<std::string, std::string> &m);
void writeSetToFile(const std::set<std::string> &s);
void writeVectorToFile(const std::vector<std::string> &v);
void writeMapToFile(const std::map<std::string, std::string> &m);

void printContent(std::map<std::string, std::string> &m);
void printNephiContent(const std::map<std::string, std::vector<std::string>> &m);
void printContentAssociatedWithKey(std::map<std::string, std::vector<std::string>> &m);

#endif
