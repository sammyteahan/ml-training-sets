#include <set>
#include <map>
#include "functions.hpp"

/**
 * Main
 *
 * main is considered `client` code. It's really nice if our client
 * can just use functions/classes defined elsewhere.
 *
 * note, these parts need to be run separately. so to see part II,
 * simply comment out lines from part I, and re-compile using `make`
 *
 * @example
 * ./program _nephi_trump.txt
 */
int main(int argc, char **argv) {
  /**
   * Part I
   */
  // std::string filename = getFileFromStandardInput(argv);
  // std::set<std::string> words = readFileToSet(filename);
  // writeSetToFile(words);

  /**
   * Part II
   */
  // std::string filename = getFileFromStandardInput(argv);
  // std::vector<std::string> words = readFileToVector(filename);
  // writeVectorToFile(words);

  /**
   * Part III
   */
  // std::string filename = getFileFromStandardInput(argv);
  // std::vector<std::string> words = readFileToVector(filename);
  // std::map<std::string, std::string> wordMap(generateMap(words));
  // writeMapToFile(wordMap);

  /**
   * Part IV
   */
  // printContent(wordMap);

  /**
   * Part V
   */
  std::string filename = getFileFromStandardInput(argv);
  std::vector<std::string> words = readFileToVector(filename);
  std::map<std::string, std::vector<std::string>> wordMap(generateBetterMap(words));
  printContentAssociatedWithKey(wordMap);

  return 0;
}
